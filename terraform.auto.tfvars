region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-0d65286608722d52b"

ami-bastion = "ami-01222e47cfe038c7f"

ami-nginx = "ami-0fb4753d70043e104"

ami-sonar = "ami-0336cb1e61ecf582a"

keypair = "Learning-Darey"

db-password = "kunle-learning"

db-username = "kunle"

account_no = "567191237150"

tags = {
  Owner-Email     = "klawluv@gmail.com"
  Managed-By      = "Terraform"
  Billing-Account = "567191237150"
}